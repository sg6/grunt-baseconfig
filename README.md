# Grunt

Grunt is a task runner, written in JavaScript.
It helps automating repetitive processes, such as restarting server on file change, or compiling scss to css.
For further information about Grunt and task runners, please visit: https://gruntjs.com/

# Config

This is my personal base config. It has 2 purposes:

* Compiling scss into css when changing a scss file (and on startup). The source folder is defined as scss, the destination as css; the folders can be changed in the config file.
* Restarting node express server when changing a js file (and on startup). The entry point is defined as index.js and can be changed in the config file.

The config can be found in the Gruntfile.js

# Install

1. Clone the repository, or copy the contents of the `package.json` and `Gruntfile.js` files.
2. Run `npm i -g grunt grunt-cli` for installing grunt globally.
3. Run `npm i` for installing the local npm dependencies.
4. Run `grunt` and grunt should automatically start.


# Changelog

* 2017-12-16: Removed grunt-contrib-sass, used grunt-sass instead; performance is way better, compiling is around 10 times faster. There is no ruby dependency needed on the OS, only Node
