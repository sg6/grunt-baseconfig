module.exports = function (grunt) {

    grunt.initConfig({
        sass: {                              // Task
            dist: {                            // Target
                options: {                       // Target options
                    style: 'expanded'
                },
                files: [{
                    expand: true,
                    cwd: 'scss/',
                    src: ['custom.scss'],
                    dest: 'css/',
                    ext: '.css'
                }]
            }
        },
        express: {
            dev: {
                options: {
                    script: './index.js'
                }
            }
        },
        watch: {
            scripts: {
                files: ['**/*.scss'],
                tasks: ['sass'],
                options: {
                    spawn: false
                }
            },
            express: {
                files: ['**.js'],
                tasks: ['express:dev'],
                options: {
                    spawn: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express-server')

    grunt.registerTask('default', ['express:dev', 'sass', 'watch']);

};
